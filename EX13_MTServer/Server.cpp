#include "Server.h"
#include <exception>
#include <iostream>
#include <string>


/*
Constructs the current server class by creating the server socket on TCP and IP stream
*/
Server::Server()
{
	//creates a server socket on IP and TCP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


/*
Destructs the current server class by closing the server socket
*/
Server::~Server()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}


/*
The function binds the server socket and makes it available for listening. Also starts recieving clients requests
Input: the port of the server
*/
void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	serverMain();  //starts accepting clients and handle their requests
}


/*
The main thread of the server. creates a thread which writes the msgs from the queue to files, and accepts logged in users
*/
void Server::serverMain()
{
	//the function which writes and creates files of the users conversations
	std::thread fileWriter(&Server::writeFile, this);
	fileWriter.detach();

	while (true)
	{
		try
		{
			std::cout << "Waiting for client connection request" << std::endl;
			accept();
		}
		catch (std::exception& e)
		{
			std::cout << "Error occured: " << e.what() << std::endl;
		}
	}
}


/*
Accepts the current logined user and creates his own thread
*/
void Server::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET clientSocket = ::accept(_serverSocket, NULL, NULL);

	if (clientSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - accept client");

	//The function which connects the client to the server
	std::string currUser = loginPhase(clientSocket);

	// the function that handle the conversation with the client
	std::thread currClientHandler(&Server::clientHandler, this, clientSocket, currUser);
	currClientHandler.detach();
}


/*
The function exacutes the login phase with the client who tries to connect to the server
Input: the socket of the client who tries to connect
Output: the name of the client who tries to connect
*/
std::string Server::loginPhase(SOCKET clientSocket)
{
	int code = Helper::getMessageTypeCode(clientSocket);  //extracts the code out of the login msg

	if (code != MT_CLIENT_LOG_IN)  //incase the first message code from the current user isnt 200 it will disconnect
	{
		closesocket(clientSocket);
		throw std::exception(__FUNCTION__ " - login client");
	}

	//extracts the user name who tried to login to the server
	int userNameLenght = Helper::getIntPartFromSocket(clientSocket, USER_NAME_LENGTH);
	std::string userName = Helper::getStringPartFromSocket(clientSocket, userNameLenght);

	//checks if the client user name is already logged in
	std::vector<std::string>::iterator pos = std::find(_userList.begin(), _userList.end(), userName);
	if (pos != _userList.end())
	{
		closesocket(clientSocket);

		std::string error = userName + " already logged in to the server";
		throw std::exception(error.c_str());
	}

	_userList.push_back(userName);  //saves the user name in the vector

	//creates the login response from the server and sends it to the client using the given client socket
	Helper::send_update_message_to_client(clientSocket, "", "", this->getAllUsersString());

	std::cout << "Client " << userName << " accepted. Server and client can speak" << std::endl;
	return userName;
}


/*
This function is a thread for each connected client and will handle his requests
Input: the client socket with the server and his user name
*/
void Server::clientHandler(SOCKET clientSocket, const std::string userName)
{	
	try
	{
		while (true)
		{
			int code = Helper::getMessageTypeCode(clientSocket);  //extracts the code out of the client msg

			if (code == MT_CLIENT_UPDATE)  //if the code is 204 - update msg from the client -> will create a msg to send back to the client
			{
				msgHandler(clientSocket, userName);
			}
			else if (code == MT_CLIENT_EXIT) //if the code is 208 - exit -> the user will get disconnected
			{
				disconnectUser(clientSocket, userName);
				break;  //ends the current thread
			}

			std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_TIME));  //sleep for 200 ms to let the client sync
		}
	}
	catch (...)  //incase of an accuring excpetion, the current user will get disconnected
	{
		disconnectUser(clientSocket, userName);
	}
}


/*
The function extracts the request fromt the given client socket and sends a fitting reply to it
Input: the client socket with the server and his user name
*/
void Server::msgHandler(SOCKET clientSocket, const std::string userName)
{
	int sendToLen = 0;
	int currMsgLen = 0;
	std::string sendTo = "";
	std::string currMsg = "";
	std::string fileContent = "";
	std::string fileName = "";
	std::string fileMsg = "";

	//gets the name of the reciever of the current msg out of the current user socket
	sendToLen = Helper::getIntPartFromSocket(clientSocket, USER_NAME_LENGTH);
	sendTo = Helper::getStringPartFromSocket(clientSocket, sendToLen);

	//gets the msg itself out of the current user socket
	currMsgLen = Helper::getIntPartFromSocket(clientSocket, MSG_LENGTH);
	currMsg = Helper::getStringPartFromSocket(clientSocket, currMsgLen);

	if (sendToLen)  //reads the file if the second user len isnt empty
	{
		fileName = this->createFileName(userName, sendTo);
		fileContent = readFile(fileName);  //puts the shared file between the two users inside this element
	}

	if (currMsgLen)  //create a msg to write to the file only if the given msg isnt empty
	{
		fileMsg = createFileMsg(currMsg, userName);  //creates the msg to add to the wanted file
		fileContent += fileMsg;

		std::unique_lock<std::mutex> msgMtx(_msgMtx);  //locks the mutex before changing the shared resource _msglist
		_msgList.push({ fileName, fileMsg });  //pushes the name of the user who recieves the msg (as first value of the pair) and the msg itself (as the second value of the pair) 
		msgMtx.unlock();

		_cond.notify_one();  //notifies the file that the queue isnt empty
	}

	Helper::send_update_message_to_client(clientSocket, fileContent, sendTo, getAllUsersString());  //creates the response of the server and sends it to the client
}


/*
The function disconnects the user from the server by deleting him from the userlist vector and closes his socket to the server
Input: the socket of the client to the server and the name of the client
*/
void Server::disconnectUser(SOCKET clientSocket, const std::string userName)
{
	//deletes the user from the userlist vector
	std::vector<std::string>::iterator pos = std::find(_userList.begin(), _userList.end(), userName);
	_userList.erase(pos);

	//closes the user socket to the server
	std::cout << userName << " disconnected" << std::endl;
	closesocket(clientSocket);
}


/*
The function creates a string which includes all of the connected users and returns it
*/
std::string Server::getAllUsersString() const
{
	std::string allUsersStr = "";

	for (std::string currUser : _userList)  //runs over all the strings that are inside the vector
	{
		allUsersStr += currUser + "&";  //saves each user name inside the string and puts the delimeter & to seperate them
	}

	return allUsersStr.substr(0, allUsersStr.size() - 1);  //returns the alluserslist string minus the last & 
}


/*
The function creates the msg that will be saved in the file accoring to the server protocol
Input: the msg to save and the author of it
Ouput: the msg but fit to the server protocol
*/
std::string Server::createFileMsg(const std::string msg, const std::string username) const
{
	//puts the protocol format msg in these string
	std::string fileMsg = FILE_MSG;
	std::string authorNamePart = AUTHOR_NAME_PART;
	std::string messageDataPart = MESSAGE_DATA_PART;

	//replpaces the string <author_name> from the format with the user name who sends the msg
	size_t pos = fileMsg.find(authorNamePart);
	size_t len = authorNamePart.length();
	fileMsg.replace(pos, len, username);

	//replpaces the string <message_data> from the format with the msg the user wants to send
	pos = fileMsg.find(messageDataPart);
	len = messageDataPart.length();
	fileMsg.replace(pos, len, msg);

	return fileMsg;
}


/*
The function creates the file name according to the two given string names
it creates the file name according to the server protocol: smaller name + "&" + bigger name
Input: the names to create the file name with
Output: the created file name according to the two given names
*/
std::string Server::createFileName(const std::string name1, const std::string name2) const
{
	//create an array with the given names and sorts it
	std::string names[] = {name1, name2};
	int len = sizeof(names) / sizeof(names[0]);
	std::sort(names, names + len);

	return std::string(names[0] + "&" + names[1] + TEXT_FILE);  //creates the file name - smaller name + "&" + bigger name + .txt ending
}


/*
The function reads the whole filename content and returns it
Input: the file to read his content
Output: the file input as a string
*/
std::string Server::readFile(const std::string fileName) const
{
	std::ifstream read(fileName);
	std::string fileInput = "";

	if (read.is_open())  //if the file is open
	{
		fileInput.assign((std::istreambuf_iterator<char>(read)), (std::istreambuf_iterator<char>()));  //reads the whole file content into a string
	}

	read.close();
	return fileInput;
}


/*
The function is a thread which reads msgs from _msgslist and writes them to the right file
*/
void Server::writeFile()
{
	std::pair<std::string, std::string> currMsg;

	while (true)
	{
		std::unique_lock<std::mutex> msgMtx(_msgMtx);  //locks the mutex before changing the shared resource _msglist

		_cond.wait(msgMtx, [&]() { return !_msgList.empty(); });  //waits for one of the clients thread to notify this thread

		//saves the first msg from the queue and removes it
		currMsg = _msgList.front();
		_msgList.pop();

		msgMtx.unlock();
		
		//opens the current file in append mode and writes the msg to it
		std::ofstream currfile(currMsg.first, std::ios::app);
		currfile << currMsg.second;
		currfile.close();
	}
}

