#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <queue>
#include <vector>
#include <thread>
#include <mutex>
#include <algorithm>
#include <iterator>
#include <fstream>
#include "Helper.h"


#define USER_NAME_LENGTH 2
#define MSG_LENGTH 5
#define TEXT_FILE ".txt"
#define FILE_MSG "&MAGSH_MESSAGE&&Author&<author_username>&DATA&<message_data>"
#define AUTHOR_NAME_PART "<author_username>"
#define MESSAGE_DATA_PART "<message_data>"
#define SLEEP_TIME 200


class Server
{
public:

	//contructor and destructor
	Server();
	~Server();

	//binds the server socket
	void serve(int port);

private:

	//client related
	void accept();
	void serverMain();
	std::string loginPhase(SOCKET clientSocket);
	void clientHandler(SOCKET clientSocket, const std::string userName);
	void msgHandler(SOCKET clientSocket, const std::string userName);
	void disconnectUser(SOCKET clientSocket, const std::string userName);

	//file related
	std::string createFileName(const std::string name1, const std::string name2) const;
	std::string readFile(const std::string fileName) const;
	void writeFile();

	//msg related
	std::string getAllUsersString() const;
	std::string createFileMsg(const std::string msg, const std::string username) const;

	//socket
	SOCKET _serverSocket;

	//containers
	std::queue<std::pair<std::string, std::string>> _msgList;
	std::vector<std::string> _userList;

	//sync related
	std::mutex _msgMtx;
	std::condition_variable _cond;
};

