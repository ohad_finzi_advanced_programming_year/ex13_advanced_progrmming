#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>
#include <fstream>


#define CONFIG_FILE "config.txt"
#define PORT_TXT "port="


int extractPort();


int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.serve(extractPort());
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}

	system("PAUSE");
	return 0;
}


/*
The function extracts the wanted port from the given config file, and returns it
*/
int extractPort()
{
	std::ifstream config(CONFIG_FILE);
	std::string currLine = "";
	std::string toFind = PORT_TXT;
	std::string port = "";
	bool flag = false;
	size_t pos;
	int i = 0;

	if (!config.is_open())
	{
		std::string error = "can't open the config file for the server";
		throw std::exception(error.c_str());
	}

	while (std::getline(config, currLine) && !flag)  //takes each line of the config file until it founds if one of the lines contains "port=" which indicates on the server port
	{
		pos = currLine.find(toFind);  //searches for the string "port=" in the current line
		if (pos != std::string::npos)  //if the string "port=" is in the current line, it will break the loop
		{
			flag = true;
		}
	}

	if (!flag)  //if the string "port=" isnt in the config file, it throws an excpetion
	{
		config.close();

		std::string error = "cant find port in the given config file";
		throw std::exception(error.c_str());
	}

	for (i = pos + toFind.size(); i < currLine.size(); i++)  //runs on the line which includes the string "port=" and extracts the port from it
	{
		if (!isdigit(currLine[i]))  //will break the loop if the current char isnt a digit
		{
			break;
		}
		 
		port += currLine[i];  //adds the current char if its a char
	}

	config.close();

	return atoi(port.c_str());  //converts the string with the numbers to digits
}

